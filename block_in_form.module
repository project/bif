<?php
/**
 * @file
 */

use \Drupal\Core\Form\FormStateInterface;
use \Drupal\block_in_form\Form\FormEntityFormDisplayEditAlter;
use \Drupal\Core\Entity\EntityInterface;
use \Drupal\Core\Entity\Display\EntityDisplayInterface;
use \Drupal\block_in_form\Form\BlockInFormViewForm;
use \Drupal\Core\Entity\ContentEntityFormInterface;
use \Drupal\Core\Form\ConfirmFormInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function block_in_form_form_entity_form_display_edit_form_alter(&$form, FormStateInterface $form_state) {
  // Only start altering the form if we need to.
  if (!empty($form['#fields']) || !empty($form['#extra'])) {
    (new FormEntityFormDisplayEditAlter($form, $form_state))->alter();
  }

  return $form;
}

/**
 * Implements hook_entity_view_alter().
 */
function block_in_form_entity_view_alter(&$build, EntityInterface $entity, EntityDisplayInterface $display) {
  if (empty($build['#blocksinform'])) {
    return;
  }

  $context = [
    'entity_type' => $display->getTargetEntityTypeId(),
    'bundle' => $entity->bundle(),
    'entity' => $entity,
    'display_context' => 'view',
    'mode' => $display->getMode(),
  ];

  (new BlockInFormViewForm())->attachBlocks($build, $context);

  // If no theme hook, we have no theme hook to preprocess.
  // Add a prerender.
  if (empty($build['#theme'])) {

    $ds_enabled = false;
    if (Drupal::moduleHandler()->moduleExists('ds')) {
      // Check if DS is enabled for this display.
      if ($display->getThirdPartySetting('ds', 'layout') && !Drupal\ds\Ds::isDisabled()) {
        $ds_enabled = true;
      }
    }

    // If DS is enabled, no pre render is needed (DS adds fieldgroup preprocessing).
    if (!$ds_enabled) {
      $build['#pre_render'][] = 'block_in_form_entity_view_pre_render';
    }
  }
}

/**
 * Pre render callback for rendering groups on entities without theme hook.
 * @param $element
 *   Entity being rendered.
 */
function block_in_form_entity_view_pre_render($element) {
  (new BlockInFormViewForm())->buildEntityBlocks($element, 'view');

  return $element;
}

/**
 * Implements hook_form_alter().
 */
function block_in_form_form_alter(array &$form, FormStateInterface $form_state) {
  $form_object = $form_state->getFormObject();
  if ($form_object instanceof ContentEntityFormInterface && !$form_object instanceof ConfirmFormInterface) {
    /**
     * @var EntityFormDisplayInterface $form_display
     */
    if ($form_display = $form_state->getStorage()['form_display']) {
      $entity = $form_state->getFormObject()->getEntity();

      $context = array(
        'entity_type' => $entity->getEntityTypeId(),
        'bundle' => $entity->bundle(),
        'entity' => $entity,
        'context' => 'form',
        'display_context' => 'form',
        'mode' => $form_display->getMode(),
      );

      $block_in_form_view_form = new BlockInFormViewForm();
      $block_in_form_view_form->attachBlocks($form, $context);
      $form['#pre_render'][] = [$block_in_form_view_form, 'formPreRender'];
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function block_in_form_preprocess_block(&$variables) {
  return $variables;
}